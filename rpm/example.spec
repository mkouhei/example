Name: example
Version: 0.1
Release: 1
Group: Utilities
Vendor: Kouhei Maeda <mkouhei@palmtb.net>
URL: https://github.com/mkouhei/example
Packager: Kouhei Maeda <mkouhei@palmtb.net>
License: GPLv3+
Summary: dummy package testing for example
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch: noarch
Source: example-0.1.tar.gz

%description
dummy package testing for example

%prep
rm -rf $RPM_BUILD_ROOT/*

%setup

%build

%install
install -m755 -d $RPM_BUILD_ROOT/usr/share/%name
install -m755 example.sh $RPM_BUILD_ROOT/usr/share/%name/
install -m755 -d $RPM_BUILD_ROOT/usr/share/doc/%name
install -m644 README $RPM_BUILD_ROOT/usr/share/doc/%name/README

%clean
rm -rf $RPM_BUILD_ROOT

%files
%attr(-, root, root) /usr/share/%name/example.sh

%doc
/usr/share/doc/%name/README

%docdir /usr/share/doc/%name
